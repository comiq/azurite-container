FROM alpine:latest

RUN apk update && \
  apk add --no-cache nodejs npm

RUN npm install -g azurite

COPY azurite.sh azurite.sh
RUN chmod ugo+x azurite.sh
ENTRYPOINT [ "/azurite.sh" ]
CMD [ "azurite" ]
